#!/bin/bash
#
# Download application binaries from Bitbucket
#

download ()
{
    owner="$1"
    repo="$2"
    file="$3"
    [[ -f "$file" ]] && mv -v "$file" "${file}.old"
    echo "Downloading https://bitbucket.org/$owner/$repo/downloads/$file"
    curl -X GET -O --location "https://bitbucket.org/$owner/$repo/downloads/$file"
    [[ "${file##*.}" != "gz" ]] && chmod a+x "$file"
}

# owner repository filename
DPC_APPS=(
    "mfkenney adread adread"
    "uwaploe bcheck bcheck"
    "uwaploe dparchiver dparchiver"
    "uwaploe dpcimm dpcimm"
    "uwaploe loadprf load_profiles"
    "uwaploe mpcmgr mpcmgr"
    "uwaploe sensormgr sensmgr"
    "mfkenney tsmem rpcfpga"
    "mfkenney tsmem tsmem"
    "mfkenney tsmem sv-ts4200.tar.gz"
)

DSC_APPS=(
    "uwaploe dparchiver dparchiver"
    "uwaploe dpdbstore dpdbstore"
    "uwaploe dpccmds dpccmds"
    "uwaploe dpview dpview"
    "uwaploe dscimm dscimm"
    "uwaploe loadprf load_profiles"
)

list=()
case "$1" in
    dpc)
        list=("${DPC_APPS[@]}")
        ;;
    dsc)
        list=("${DSC_APPS[@]}")
        ;;
    *)
        echo "Usage: $(basename $0) dpc|dsc" 1>&2
        exit 1
        ;;
esac

if [[ "$2" = "dist" ]]; then
    d="${1}-dist/bin"
    mkdir -p "$d"
    cd "$d"
else
    cd $HOME/bin
fi

for app in "${list[@]}"; do
    download $app
done

# Install tsmem client and server
if [[ "$2" != "dist" && -f tsmem ]]; then
    sudo mv -v tsmem /usr/local/bin
    sudo mv -v rpcfpga /usr/local/bin
    sudo tar -x -v -z -C /etc/sv -f - < sv-ts4200.tar.gz && \
        rm -f sv-ts4200.tar.gz
fi
