#!/bin/bash
#
# Switch power to the various sensors attached to the DPC.
#
CFGDIR=$HOME/config
[ -f $CFGDIR/switches ] && . $CFGDIR/switches

power_test ()
{
    local state

    state=$(tsfpga dioget $1|cut -f2 -d= 2> /dev/null)
    [ "$state" = "HIGH" ]
}

command -v tsfpga &> /dev/null || {
    echo "Install tsfpga"
    exit 1
}

#
# If there are no command-line arguments, print the current
# state of every sensor.
#
if (($# == 0)); then
    for dev in "${!SWITCHES[@]}"; do
        if power_test "${SWITCHES[$dev]}"; then
            echo "${dev}=on"
        else
            echo "${dev}=off"
        fi
    done
    exit 0
fi

state=
for arg; do
    case "$arg" in
        -on) state="high" ;;
        -off) state="low" ;;
        *)
            sw="${SWITCHES[$arg]}"
            [[ "$sw" ]] || {
                name="${arg}_1"
                sw="${SWITCHES[$name]}"
            }
            if [[ "$sw" ]]; then
                if [[ "$state" ]]; then
                    tsfpga dioset "$sw" "$state"
                fi
                if power_test "$sw"; then
                    echo "${arg}=on"
                else
                    echo "${arg}=off"
                fi
            fi
            ;;
    esac
done
