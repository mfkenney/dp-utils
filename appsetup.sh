#!/bin/bash
#
# Perform initial setup of DP applications
#

# Link scripts into service directory
svcs=($HOME/sv/*)
cd $HOME/service
for sdir in "${svcs[@]}";do
    s="${sdir##*/}"
    [[ -h $s ]] || {
        echo "Installing service: $s"
        ln -s ../sv/$s .
        sleep 1
    }
done

# Report service status
for sdir in "${svcs[@]}";do
    s="${sdir##*/}"
    [[ -h $s ]] && {
        sv status ./$s
    }
done

[[ -f $HOME/jobs.cron ]] && {
    echo "Starting cron jobs"
    crontab $HOME/jobs.cron
}
