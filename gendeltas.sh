#!/bin/bash
#
# Generate rdiff delta files for DP applications. The corresponding
# signature files must be in the current directory and must be named:
#
#     <appname>.sig
#
# For each signature file, this script will generate the following:
#
#     <appname>.delta.gz  compressed patch file
#     <appname>.sha1sum   SHA-1 hash of the new app binary
#

if which sha1sum 1> /dev/null 2>&1; then
    SHASUM=sha1sum
else
    SHASUM="shasum -a 1"
fi

for sig in *.sig; do
    app="${sig%.*}"
    path="$GOPATH/bin/${app}-xc/snapshot/linux_arm/$app"
    if [[ -f $path ]]; then
        delta="${app}.delta"
        echo "Generating patch file for $app"
        rdiff delta "$sig" "$path" "$delta" && gzip "$delta"
        # We don't want the checksum output to contain the full
        # path so cd to the app directory before generating it.
        (
            cd "${path%/*}"
            $SHASUM "$app"
        ) > "${app}.sha1sum"
    else
        echo "Application $app not found"
    fi
done
