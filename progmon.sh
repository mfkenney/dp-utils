#!/bin/bash
#
# Collect memory statistics on various DPC application programs
#

PROGS=("dpcimm" "mpcmgr" "sensmgr" "dparchiver" "redis-server")
LOGDIR=$HOME/logs

mkdir -p $LOGDIR
for prog in "${PROGS[@]}"; do
    pid=$(pidof $prog)
    [[ -n $pid ]] && \
        cat /proc/$pid/stat >> $LOGDIR/${prog}.stats
done
