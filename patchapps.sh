#!/bin/bash
#
# Apply a binary patch to one or more DP application files. The
# patches and associated checksums must be stored in the current
# directory.
#
#     <appname>.delta.gz  compressed patch file
#     <appname>.sha1sum   SHA-1 hash of the new app binary
#

BINDIR=$HOME/bin
if which sha1sum 1> /dev/null 2>&1; then
    SHASUM=sha1sum
else
    SHASUM="shasum -a 1"
fi

set -e
for patch in *.delta.gz; do
    app="${patch%.delta.gz}"
    path="$BINDIR/$app"
    if [[ -f $path ]]; then
        gunzip "$patch" && \
            rdiff patch "$path" "${app}.delta" "$app" &&\
            chmod a+x "$app"
        if $SHASUM -c "${app}.sha1sum"; then
            echo "Installing updated $app"
            mv -v "$app" "${BINDIR}/"
        else
            echo "Checksum failed for $app"
        fi
    else
        echo "Application $app not found"
    fi
done
