#!/bin/bash
#
# Create git bundles to download to a DPC or DSC system.
#
# Read a list of repository names and revision IDs from standard
# input and creates a compressed tar archive of bundles to bring
# each repo up to date.
#

: ${PROJDIR=$HOME/projects/RSN-DP}

BASE=$TMPDIR/bundles
rm -rf $BASE
mkdir -p $BASE

n=0
while read repo rev; do
    dir="$PROJDIR/$repo"
    if [[ -d "$dir" ]];then
        :
    else
        case "$repo" in
            rsn-pydp) dir="$PROJDIR/pydp"
                      ;;
            dp-utils) dir="$PROJDIR/utils"
                      ;;
        esac
        [[ -d "$dir" ]] || continue
    fi
    echo "Checking $dir ... "
    b=$(GIT_DIR="$dir/.git" git rev-parse --abbrev-ref HEAD)
    GIT_DIR="$dir/.git" git bundle create "$BASE/${repo}.bundle" ${rev}..${b}\
        && ((n++))
done

arname="dp-bundles.tar.gz"
if ((n > 0)); then
    echo "Creating archive $arname"
    tar -C $TMPDIR -c -v -z -f $arname bundles
else
    echo "All repos are up-to-date"
fi
