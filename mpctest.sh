#!/usr/bin/env bash
#
# Test the MPC by running a short stationary profile.
#
PATH=$HOME/bin:$PATH
export PATH

duration=${1:-120}
profile="/tmp/decktest.yaml"

cat<<EOF > $profile
---
- dir: STATIONARY
  maxtime: $duration
EOF

echo "Running a stationary profile for $duration seconds ..."

load_profiles -device=dpc "$profile"
rm -f $profile
coproc tail -n 1 -f $HOME/logs/mmpmgr/current
trap "kill $COPROC_PID;exit 1" 0 1 2 3 15

pnum=
while read line
do
    echo "$line"
    [[ "$line" =~ powering\ off ]] && break
    [[ "$line" =~ profile:start ]] && {
        eval "$(cut -f4- -d' ' <<< "$line" | tr ' ' ';')"
    }
done <&"${COPROC[0]}"

[[ -n "$pnum" ]] && {
    echo "Data Archive listing ..."
    ls -l ~/data/*_${pnum}
}
