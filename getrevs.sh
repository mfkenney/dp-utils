#!/bin/bash
#
# Get the latest revision ID for all git repositories on a
# DPC or DSC system.
#

dirs=($HOME/src/*)
for d in "${dirs[@]}"; do
    echo -n "${d##*/} "
    GIT_DIR="$d/.git" git log --oneline -1|cut -f1 -d' '
done
