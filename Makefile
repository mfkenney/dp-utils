#
# Makefile to install utility scripts.
#
SHELL = /bin/bash
INSTALL = install
BINDIR = $(HOME)/bin
DPC_SCRIPTS = power.sh appsetup.sh getrevs.sh patchapps.sh \
	tswatch.sh getapps.sh mpctest.sh
DSC_SCRIPTS = power.sh appsetup.sh getrevs.sh ips tswatch.sh \
	getapps.sh

.PHONY: dpc dsc

dpc: $(DPC_SCRIPTS)
	$(INSTALL) -d $(BINDIR)
	$(INSTALL) -m 755 $^ $(BINDIR)/
	ln -sf $(BINDIR)/power.sh $(BINDIR)/power

dsc: $(DSC_SCRIPTS)
	$(INSTALL) -d $(BINDIR)
	$(INSTALL) -m 755 $^ $(BINDIR)/
	ln -sf $(BINDIR)/power.sh $(BINDIR)/power
